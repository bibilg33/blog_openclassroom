<?php 

require('controller/frontend.php');

try
{
    if(isset($_GET['action'])) // If the URL contain 'action'
    {
        if($_GET['action'] == 'listPosts') // if action = 'listPosts' in URL
        {
            listPosts(); // function who get data and display 5 posts
        }
        elseif($_GET['action'] == 'post') // if action = 'post' in URL
        {
            if(isset($_GET['billet']) && $_GET['billet'] > 0) // If the URL contain 'action' and 'billet // TO DO : Test if id is in db
            {
                post(); // function who get data and display 1 post and his comments
            }
            else // If the URL DONT contain 'billet
            {
                throw new Exception('Aucun billet envoyé'); // CANT DISPLAY
            }
        }
        elseif($_GET['action'] == 'addComment') // if action = 'addComment' in URL
        {
            if(isset($_POST['pseudo']) && isset($_POST['message'])) // Need to contain $_POST
            {
                if(!empty($_POST['pseudo']) && !empty($_POST['message'])) // If datas are not empty
                {
                    addComment($_POST['pseudo'],$_POST['message'],$_POST['id']); /* Function which add comments on db and
                    redirect with action=post and the id of billet */
                }
                else 
                {
                    throw new Exception('Les champs ne doivent pas être vides'); // CANT DISPLAY
                }
            }
            else
            {
                throw new Exception('Erreur dans l\'ajout du message'); // CANT DISPLAY
            }
        }
        elseif($_GET['action'] == 'editComment') 
        {
            if(isset($_GET['idComment']) ) // How and where to test if idComment is in Database ?
            {
                if(isset($_POST['newContent'])) // Here, the form has been send, we try to edit comment
                {
                    if(!empty($_POST['newContent'])) // If the comment is not empty
                    {
                        editComment($_POST['newContent']); // We can edit Comment
                    }
                    else
                    {
                        throw new Exception('Erreur : aucun contenu dans le nouveau message'); // If the comment is empty
                    }
                }
                else // Here, the form don't has been send, we just display the comment to edit
                {
                    displayEditComment(); // We just display the comment to edit
                }    
            }
            else
            {
                throw new Exception('Erreur : pas d\'id de billet renseigné'); // CANT DISPLAY
            }
        }
    }
    else
    {
        listPosts(); // If the URL has not parameters
    }
}
catch(exception $e)
{
    $errorMessage = $e->getMessage();
    Require('view/frontend/errorView.php');
}