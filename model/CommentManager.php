<?php

require_once("model/Manager.php");

class CommentManager extends Manager
{

    function postComment($pseudo,$message,$idBillet) // Add a row for commentaires in db
    {
        $db=$this->dbConnect();
    
        $request=$db->prepare('INSERT INTO commentaires(author,comment,id_billet,comment_date) VALUES (:author,:message,:id_billet,NOW())');
        $affectedLines=$request->execute(array(
            'author' => $pseudo,
            'message' => $message,
            'id_billet' => $idBillet
    
        ));
    
        return $affectedLines; // true if it's ok, false if don't ok
    }
    
    function getComments($postID) // Returns the comments of a post according to its ID
    {
        $db=$this->dbConnect(); 
    
        $comments=$db->prepare('SELECT C.id AS idComment, C.author AS author, C.comment AS comment, C.comment_date AS comment_date
                FROM billets AS B, commentaires AS C 
                WHERE C.id_billet = B.id AND B.id = :id');
    
                $comments->execute(array(
                    'id' => $postID
                ));
    
                return $comments;
    }    

    function getAuthor($commentId) // Return the Author of a comment thanks to it's id
    {
        $db=$this->dbConnect(); 

        $author=$db->prepare('SELECT author
                FROM commentaires 
                WHERE id = :id');
    
                $author->execute(array(
                    'id' => $commentId
                ));

                $result=$author->fetch();
    
                return $result['author'];
    }

    function getComment($commentId) // Return the content of a comment thanks to it's id
    {
        $db=$this->dbConnect(); 

        $comment=$db->prepare('SELECT comment
                FROM commentaires 
                WHERE id = :id');
    
                $comment->execute(array(
                    'id' => $commentId
                ));

                $result=$comment->fetch();
    
                return $result['comment'];
    }

    function setComment($id,$newContent) // Set a comment 
    {
        $db=$this->dbConnect(); 

        $content=$db->prepare('UPDATE commentaires
                SET comment = :newContent
                WHERE id = :id');

        $affectedLines=$content->execute(array(
            'newContent' => $newContent,
            'id' => $id
        ));

        return $affectedLines; // True if there is no problems
    }

    function getBilletId($commentId) // Return the Billet's Id of a comment(thanks the commentId)
    {
        $db=$this->dbConnect();

        $comment=$db->prepare('SELECT id_billet
                FROM commentaires 
                WHERE id = :id');
    
                $comment->execute(array(
                    'id' => $commentId
                ));

                $result=$comment->fetch();
    
                return $result['id_billet'];
    }
}
