<?php 

require_once("model/Manager.php");

class PostManager extends Manager
{


    function getPosts() // Return the information for 5 posts in db
    {
        $db=$this->dbConnect();
        
        $request=$db->query('SELECT id,title,content,DATE_FORMAT(creation_date, \'%d/%m/%Y à %Hh%imin%ss\') AS creation_date_fr 
        FROM billets ORDER BY id LIMIT 5');
        
        return $request;
    }
    
    function getPost($postId) // Returns the information of a post according to its id
    {
        $db=$this->dbConnect();
    
        $request=$db->prepare('SELECT title, content, DATE_FORMAT(creation_date, \'%d/%m/%Y à %Hh%imin%ss\') AS creation_date_fr 
        FROM billets WHERE id = :id');
        $request->execute(array(
            'id' => $postId
        ));
    
        $resultat=$request->fetch();
    
        return $resultat;
    }
    
}