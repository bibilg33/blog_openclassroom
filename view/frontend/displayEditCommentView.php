<?php $title = 'Modification de Commentaire';?>

<?php ob_start(); ?>
        <h1>Mon blog !</h1>

        <p><a href="index.php">Retour à la liste des billets</a></p>

        <h2>Modifier le commentaire</h2>

        <form action="index.php?action=editComment&idComment=<?= $commentId?>" method="post">
            <p>
                Pseudo de l'auteur : <?= $pseudo ?> 
            </p>
            <p>
                <label for="newContent">Message à modifier :</label>
                    <textarea type="text" id="newContent" name="newContent" rows="5"><?= $content ?></textarea>
            </p>  
                    <!-- Send hidden date with type hidden-->
                    <input type="submit" value="Modifier" />
        </form>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>

