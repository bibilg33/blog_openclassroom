<?php $title = 'Commentaire de ' . $post['title']?>

<?php ob_start(); ?>
        <h1>Mon blog !</h1>

        <p><a href="index.php">Retour à la liste des billets</a></p>

        <div class = "news">
            <h3> <?= $post['title'] ?> le <?= $post['creation_date_fr'] ?></h3>

            <p> <?= nl2br(htmlspecialchars($post['content']))?> </br> </p>
                    
        </div>

        <h2>Commentaires :</h2>

        <?php 
            while($comment=$comments->fetch())
            {
        ?>
            <p> 
                <?= htmlspecialchars($comment['author'])?> le <?= $comment['comment_date']?>  
                <a href="index.php?action=editComment&idComment=<?=$comment['idComment']?>">(Modifier)</a>   
            </p> 
            <p><?= nl2br(htmlspecialchars($comment['comment']))?></p>
        <?php
            }
        ?>

        <h2>Ajouter un commentaire</h2>

        <form action="index.php?action=addComment&amp;billet=<?= $_GET['billet']?>" method="post">
            <p>
                <label for="pseudo">Pseudo :</label>
                    <input type="text" id="pseudo" name="pseudo" /> <!-- l'attribut id est complémentaire à la balise for de label -->
            </p>
            <p>
                <label for="message">Votre message :</label>
                    <textarea type="text" id="message" name="message" rows="5"></textarea>
            </p>
                    <input name="id" type="hidden" value="<?= $_GET['billet']?>"/> 
                    <!-- Send hidden date with type hidden-->
                    <input type="submit" value="Valider" />
        </form>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>