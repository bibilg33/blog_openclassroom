<!-- Affichage -->

<?php $title = 'Blog'; ?>

<?php ob_start(); ?>
        <h1>Mon blog !</h1>
        <h2>Derniers billets :</h2>

        <?php 
        while($resultat= $request->fetch())
        {
        ?>
            <div class = "news">
                <h3> 
                    <?= htmlspecialchars($resultat['title']) ?> 
                    le <?= $resultat['creation_date_fr'] ?>
                </h3> 

                <p> 
                    <?= nl2br(htmlspecialchars($resultat['content'])) ?> <!-- nl2br convertit les retours à la ligne 
                    en html </br>, htmlspecialchars permet de protéger les textes -->

                    </br>
                    <a href="index.php?action=post&amp;billet=<?= $resultat['id']?>"> Commentaires </a>
                        
                </p>
            </div>

        <?php
        }

        $request->closeCursor();
        ?>
<?php $content = ob_get_clean(); ?>                      

<?php require('template.php'); ?>