<?php 

require_once('model/PostManager.php');
require_once('model/CommentManager.php');

function editComment($newContent) // Function how edit comment and redirect on the good location
{
    $commentManager = New CommentManager();

    $commentId=$_GET['idComment'];

    $affectedLines = $commentManager->setComment($commentId,$newContent); // Update a comment

    $idBillet= $commentManager->getBilletId($commentId); // Get billet's id

    if ($affectedLines === false) 
    {
       throw new Exception('Impossible de modifier le commentaire !'); 
    }
    else 
    {
        header('Location: index.php?action=post&billet=' . $idBillet); // Redirect on the post
    }
}

function displayEditComment() // Function how display one comment for edit him
{
    $commentManager = New CommentManager();

    $commentId=$_GET['idComment'];
    $pseudo = $commentManager->getAuthor($commentId);
    $content = $commentManager->getComment($commentId);

    require('view/frontend/displayEditCommentView.php');
}

function listPosts() // old index.php --> display principal page with list of posts
{
    $postManager = New PostManager();
    
    $request = $postManager->getPosts();

    require('view/frontend/indexView.php');
}

function post() // Old comment.php --> Display ONE post and his comments
{
    $postManager = New PostManager();
    $commentManager = New CommentManager();

    $post=$postManager->getPost($_GET['billet']); // function who get data to display one post
    $comments=$commentManager->getComments($_GET['billet']); // function who get data to display comments of one post

    require('view/frontend/commentView.php'); 
}

function addComment($pseudo,$message,$idBillet) /* Function which add comments on db and
redirect with action=post and the id of billet */
{
    $commentManager = New CommentManager();

    $affectedLines=$commentManager->postComment($pseudo,$message,$idBillet); // afectedLines is true if the comment is correctly add in db

    if ($affectedLines === false) 
    {
       // die('Impossible d\'ajouter le commentaire !'); we can remove because the exceptions are managed in the router
       throw new Exception('Impossible d\'ajouter le commentaire !'); 
    }
    else 
    {
        header('Location: index.php?action=post&billet=' . $idBillet); // Redirect on the post
    }

}